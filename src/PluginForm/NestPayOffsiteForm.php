<?php

namespace Drupal\commerce_nestpay\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use CommerceGuys\Intl\Currency\CurrencyRepository;

/**
 * {@inheritdoc}
 */
class NestPayOffsiteForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $payment = $this->entity;
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $currencyRepository = new CurrencyRepository();
    $currency = $currencyRepository->get($payment->getAmount()->getCurrencyCode());

    $configuration = $payment_gateway_plugin->getConfiguration();
    $mode = $configuration['mode'];

    $store_type = '3d_pay_hosting';
    $client_id = $configuration['client_id'];
    $store_key = $configuration['store_key'];
    $rnd = bin2hex(random_bytes(6));
    $currency_code = $currency->getNumericCode();
    $oid = $payment->getOrderId();
    $amount = number_format((float) $payment->getAmount()->getNumber(), 2, '.', '');
    $ok_url = $form['#return_url'];
    $fail_url = $form['#cancel_url'];
    $transaction_type = 'PreAuth';
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $hash_algorithm = 'ver2';

    $plain_text = $client_id . '|' . $oid . '|' . $amount . '|' . $ok_url . '|'
      . $fail_url . '|' . $transaction_type . '||' . $rnd
      . '||||' . $currency_code . '|' . $store_key;

    $hashValue = hash('sha512', $plain_text);
    $hash = base64_encode(pack('H*', $hashValue));

    $data = [
      'clientid' => $client_id,
      'storetype' => $store_type,
      'hash' => $hash,
      'trantype' => $transaction_type,
      'amount' => $amount,
      'currency' => $currency_code,
      'oid' => $oid,
      'okUrl' => $ok_url,
      'failUrl' => $fail_url,
      'lang' => $lang,
      'rnd' => $rnd,
      'encoding' => 'utf-8',
      'hashAlgorithm' => $hash_algorithm,
      'shopurl' => \Drupal::request()->getSchemeAndHttpHost(),
    ];

    $link = 'https://bib.eway2pay.com/fim/est3Dgate';

    if ($mode == 'test') {
      $link = 'https://testsecurepay.eway2pay.com/fim/est3dgate';
    }

    return $this->buildRedirectForm(
      $form,
      $form_state,
      $link,
      $data,
      PaymentOffsiteForm::REDIRECT_POST
    );

  }

}
